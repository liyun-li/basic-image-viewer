import { Box, createMuiTheme, createStyles, Grid, Hidden, makeStyles, MuiThemeProvider, Theme, Toolbar } from '@material-ui/core'
import React from 'react'
import ImageHolder from './ImageHolder'
import Navigation from './Navigation'
import SideMenu from './SideMenu'

const theme = createMuiTheme({
  palette: {
    type: 'dark'
  }
})

const useStyles = makeStyles((_theme: Theme) => createStyles({
  container: {
    height: '100vh'
  }
}))

const App = () => {
  const { container } = useStyles()

  return (
    <MuiThemeProvider theme={theme}>
      <Navigation />
      <Grid container className={container}>
        <Hidden smDown>
          <Grid item md={3} lg={2}>
            <SideMenu />
          </Grid>
        </Hidden>
        <Grid item md={9} lg={10} xs={12}>
          <Box display='flex' height='100%' flexDirection='column'>
            <Toolbar />
            <ImageHolder />
          </Box>
        </Grid>
      </Grid>
    </MuiThemeProvider>
  )
}

export default App