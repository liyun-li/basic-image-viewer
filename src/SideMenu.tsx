import { createStyles, List, ListItem, makeStyles, Paper, Theme, Toolbar } from '@material-ui/core'

const useStyles = makeStyles((theme: Theme) => createStyles({
  root: {
    height: '100%'
  },

  drawer: {
    width: '100%',
    zIndex: theme.zIndex.appBar - 1
  }
}))

const SideMenu = () => {
  const { root, drawer } = useStyles()

  return (
    <Paper square className={root}>
      <Toolbar />
      <List className={drawer}>
        <ListItem>
        </ListItem>
      </List>
    </Paper>
  )
}

export default SideMenu