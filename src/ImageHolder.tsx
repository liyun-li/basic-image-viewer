import { Box, createStyles, Grid, makeStyles, Theme } from '@material-ui/core'
import React, { FC, MutableRefObject, useEffect, useRef } from 'react'
import logo from './images/logo640.png'
import { useAppSelector } from './redux/hooks'
import { ImageFile } from './redux/slices/image'

const useStyles = makeStyles((_theme: Theme) => createStyles({
  root: {
    backgroundImage: `url(${logo})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'contain'
  },

  canvas: {
    maxWidth: '100%'
  }
}))

const Canvas: FC<{ image: ImageFile }> = ({ image }) => {
  const canvasRef = useRef() as MutableRefObject<HTMLCanvasElement>
  const { canvas: canvasStyle } = useStyles()

  useEffect(() => {
    const canvas = canvasRef.current
    const context = canvas.getContext('2d')!
    const img = new Image()

    img.src = image.data
    img.onload = () => {
      const imageData = context.getImageData(0, 0, img.width, img.height).data

      canvas.width = img.width
      canvas.height = img.height
      context.drawImage(img, 0, 0)
    }
  })

  return (
    <Box flex='auto'>
      <Grid container alignItems='center' justify='center' style={{ height: '100%' }}>
        <canvas ref={canvasRef} className={canvasStyle} />
      </Grid>
    </Box>
  )
}

const ImageHolder: FC = () => {
  const { root } = useStyles()
  const image = useAppSelector(state => state.image)

  return /image\/.*/.test(image.type)
    ? <Canvas image={image} />
    : <Box flex='auto' className={root} />
}

export default ImageHolder