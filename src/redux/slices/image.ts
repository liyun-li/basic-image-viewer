import { createSlice, PayloadAction } from '@reduxjs/toolkit'

export interface ImageFile {
  name: string,
  type: string,
  data: string
}

export const slice = createSlice({
  name: 'image',
  initialState: {
    name: '',
    type: '',
    data: ''
  },
  reducers: {
    importImage: (_, action: PayloadAction<ImageFile>) => action.payload
  }
})

export default slice.reducer

export const { importImage } = slice.actions