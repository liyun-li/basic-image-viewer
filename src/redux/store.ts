import { createStore } from 'redux'
import { combineReducers } from 'redux'
import imageReducer from './slices/image'

const rootReducer = combineReducers({
  image: imageReducer
})

const store = createStore(rootReducer)

export default store

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch