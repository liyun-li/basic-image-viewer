import { Button, createStyles, makeStyles, Theme } from '@material-ui/core'
import React from 'react'

const useStyles = makeStyles((theme: Theme) => createStyles({
  button: {
    padding: theme.spacing(1),
    paddingLeft: theme.spacing(2)
  }
}))

const AppButton = (text: string) => {
  const { button } = useStyles()
  
  return (props: any) => (
    <Button className={button} disableFocusRipple {...props}>
      {text}
    </Button>
  )
}

export {
  AppButton
}