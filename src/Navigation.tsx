import { AppBar, Button, Grid, Toolbar, Typography } from '@material-ui/core'
import { CloudDownload, CloudUpload, PhotoCamera, PhotoLibrary } from '@material-ui/icons'
import React, { FC, MutableRefObject, useRef } from 'react'
import { AppButton } from './hoc/buttons'
import { useAppDispatch } from './redux/hooks'
import { importImage } from './redux/slices/image'

const Navigation: FC = () => {
  const inputFile = useRef() as MutableRefObject<HTMLInputElement>

  const UploadButton = AppButton('Import')
  const ExportButton = AppButton('Export')

  const dispatch = useAppDispatch()

  return (
    <AppBar>
      <Toolbar>
        <Grid container alignItems='center'>
          <Grid item xs={3}>
            <Button startIcon={<PhotoCamera />} endIcon={<PhotoLibrary />} disableRipple>
              <Typography variant='h6'>Image Viewer</Typography>
            </Button>
          </Grid>
          <Grid item xs={9}>
            <Grid container justify='flex-end' spacing={2}>
              <Grid item>
                <ExportButton startIcon={<CloudDownload />} />
              </Grid>
              <Grid item>
                <input type='file' id='upload' style={{ display: 'none' }} ref={inputFile} accept='image/*'
                  onChange={e => {
                    const file: File = e.target.files![0]
                    const fileReader = new FileReader()
                    fileReader.readAsDataURL(file)
                    fileReader.onloadend = () => {
                      const { name, type } = file
                      const data = fileReader.result as string
                      dispatch(importImage({
                        name,
                        type,
                        data
                      }))
                    }
                  }} />
                <UploadButton startIcon={<CloudUpload />} onClick={() => inputFile.current.click()} />
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  )
}

export default Navigation